=Settings Management=

This project is Django settings selection. You can split any settings
as a shared properties and seperately properties.
The setting will be loaded by the ENV environment variable.

==Production==
===For *nix OS setting===
$ export ENV=PROD
To see all environment's variables
$ env

===For window OS===
> set ENV=PROD

==Development==