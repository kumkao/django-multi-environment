from .base import *

DEBUG = False
ALLOWED_HOSTS = ['*']
INSTALLED_APPS += (

)

SECRET_KEY = 'YOUR_SECRET_HERE'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': '',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

TEMPLATES[0]['OPTIONS']['debug'] = DEBUG

# APP CUSTOM CONFIGURATION
SR = {
    'fb': {
        'appid': 'your_id_of_3rd_party'
    }
}