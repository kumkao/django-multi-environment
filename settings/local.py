from .base import *

DEBUG = True
ALLOWED_HOSTS = []
INSTALLED_APPS += (

)

SECRET_KEY = 'YOUR_SECRET_HERE'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': './db.sqlite3',
    }
}

TEMPLATES[0]['OPTIONS']['context_processors'] += [
    'django.template.context_processors.debug'
]
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG

# APP CUSTOM CONFIGURATION
SR = {
    'fb': {
        'appid': 'your_id_of_3rd_party'
    }
}